/**
 * Dion Gerrese
 */

class DNA {
    constructor(genes) {
        if (genes) {
            this.genes = genes;
        } else {
            this.genes = [];
            for (let i = 0; i < lifespan; i++) {
                this.genes[i] = p5.Vector.random2D();
            }
        }
    }

    mutate() {
        for (let index in this.genes) {
            if (random(1) < mutationFactor) {
                if (!this.genes.hasOwnProperty(index)) {
                    continue;
                }

                this.genes[index] = p5.Vector.random2D();
                this.genes[index].setMag(maxForce);
            }
        }
    }

    crossover(partner) {
        let newGenes = [];

        for (let i = 0; i < this.genes.length; i++) {
            if (random(1) > 0.5) {
                newGenes[i] = this.genes[i];
            } else {
                newGenes[i] = partner.genes[i];
            }
        }

        return new DNA(newGenes);
    }
}
