var lifespan = 400;
var step = 0;
var generation = 0;
var mutationFactor = 0.0005;
var maxForce = 0.0001;

var target;
var population;
var obstacles = [];
var bestDistance;
var bestTime = lifespan;
var bestPath = [];

var fittestCounter;
var generationCounter;

function setup() {
    createCanvas(600, 800);

    target = createVector(width / 2, 50);
    population = new Population(1000);

    bestDistance = height;

    fittestCounter = createP();
    generationCounter = createP();
}

function draw() {
    background(0);
    drawTarget();

    drawObstacles();

    let hasAlivePopulation = population.run();
    fittestCounter.html('Best time:' + bestTime + ' | Best distance:' + bestDistance);
    generationCounter.html('Generation:' + generation + ' | Step: ' + step + '/' + lifespan);

    for (let pathNode of bestPath) {
        push();
        translate(pathNode.x, pathNode.y);
        fill(66, 255, 255, 255);
        ellipse(0, 0, 2);
        pop();
    }

    tick(hasAlivePopulation);
}

function tick(hasAlivePopulation) {
    step++;
    if (!hasAlivePopulation || step === lifespan) {
        population.evaluate();
        population.evolve();

        step = 0;
        generation++;
    }
}

function drawObstacles() {
    rect(150, 250, 300, 10);
    rect(0, 400, 200, 10);
    rect(400, 400, 200, 10);
}

function drawTarget() {
    push();
    fill(100, 0, 255, 255);
    ellipse(target.x, target.y, 10);
    pop();
}
