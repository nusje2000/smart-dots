class Population {
    constructor(populationSize = 1) {
        this.populationSize = populationSize;

        this.dots = [];
        this.matingPool = [];

        for (let i = 0; i < this.populationSize; i++) {
            this.dots[i] = new Dot();
        }
    }

    evolve() {
        let newDots = [];
        for (let dot of this.dots) {
            let parentA = this.matingPool[Math.floor(random(0, this.matingPool.length))].dna;
            let parentB = this.matingPool[Math.floor(random(0, this.matingPool.length))].dna;

            let child = parentA.crossover(parentB);
            child.mutate();

            newDots.push(new Dot(child));
        }

        this.dots = newDots;
    }

    evaluate() {
        let maxFitness = 0;
        let fittestDot = null;
        bestTime = lifespan;

        for (let dot of this.dots) {
            dot.calculateFitness();

            if (dot.fitness > maxFitness) {
                bestPath = dot.tracePoints;
                maxFitness = dot.fitness;
                fittestDot = dot;
            }

            if (dot.finished && dot.stepsTaken < bestTime) {
                bestTime = dot.stepsTaken;
            }
        }

        for (let dot of this.dots) {
            dot.fitness /= maxFitness;
        }

        this.matingPool = [];
        for (let dot of this.dots) {
            let dominance = dot.fitness * 100;
            for (let i = 0; i < dominance; i++) {
                this.matingPool.push(dot);
            }
        }

        bestDistance = maxFitness;
    }

    run() {
        let canUpdate = false;
        let someoneFinished = false;
        for (let dot of this.dots) {
            if (dot.update()) {
                canUpdate = true;
            }

            if (dot.finished) {
                someoneFinished = true;
                break;
            }

            dot.show();
        }

        return canUpdate && !someoneFinished;
    }
}
