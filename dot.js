class Dot {
    constructor(dna) {
        this.dna = dna || new DNA();

        this.position = createVector(width / 2, height);
        this.velocity = createVector();
        this.acceleration = createVector();

        this.fitness = 0;
        this.dead = false;
        this.finished = false;
        this.stepsTaken = 0;
        this.closestDistance = 100000;
        this.startDistance = dist(this.position.x, this.position.y, target.x, target.y);
        this.currentDistance = dist(this.position.x, this.position.y, target.x, target.y);
        this.tracePoints = [];
    }

    calculateFitness() {
        let distanceToTarget = this.closestDistance;

        this.fitness = map(distanceToTarget, 0, width, width, 0);
        this.fitness **= 2;

        if (this.finished) {
            let stepsLeft = lifespan - this.stepsTaken;
            this.fitness = 10000 * (stepsLeft ** 4);
        }
    }

    applyForce(force) {
        this.acceleration.add(force);
    }

    update() {
        if (this.dead || this.finished) {
            return false;
        }

        if (this.position.x < 0 || this.position.x > width || this.position.y < 0 || this.position.y > height) {
            this.dead = true;
        }

        if (this.position.x > 150 && this.position.x < 450 && this.position.y > 250 && this.position.y < 260) {
            this.dead = true;
        }

        if (this.position.x > 0 && this.position.x < 200 && this.position.y > 400 && this.position.y < 410) {
            this.dead = true;
        }

        if (this.position.x > 400 && this.position.x < 600 && this.position.y > 400 && this.position.y < 410) {
            this.dead = true;
        }

        let distanceToTarget = dist(this.position.x, this.position.y, target.x, target.y);
        if (distanceToTarget <= 10) {
            this.finished = true;
        }

        this.currentDistance = distanceToTarget;

        if (this.closestDistance > distanceToTarget) {
            this.closestDistance = distanceToTarget;
        }

        if (!this.dead && !this.finished) {
            this.applyForce(this.dna.genes[step]);

            this.velocity.add(this.acceleration);
            this.velocity.limit(5);
            this.position.add(this.velocity);
            this.acceleration.mult(0);

            this.tracePoints.push({x: this.position.x, y: this.position.y});

            this.stepsTaken++;
        }

        return true;
    }

    show() {
        push();

        noStroke();
        if (this.dead) {
            fill(100, 100, 100, 100);
        } else if (this.finished) {
            fill(0, 225, 0, 100);
        } else {
            const color = this.getColor();

            fill(color.r, color.g, color.b, 75);
        }

        translate(this.position.x, this.position.y);
        ellipse(0, 0, 10);

        pop();
    }

    getColor() {
        let percentColors = [
            {pct: 0.0, color: {r: 0xff, g: 0x00, b: 0}},
            {pct: 0.5, color: {r: 0xff, g: 0xff, b: 0}},
            {pct: 1.0, color: {r: 0x00, g: 0xff, b: 0}}
        ];

        let pct = 1 - (this.currentDistance / this.startDistance);

        let i;
        for (i = 1; i < percentColors.length - 1; i++) {
            if (pct < percentColors[i].pct) {
                break;
            }
        }

        let lower = percentColors[i - 1];
        let upper = percentColors[i];
        let rangePct = (pct - lower.pct) / (upper.pct - lower.pct);

        return {
            r: Math.floor(lower.color.r * (1 - rangePct) + upper.color.r * rangePct),
            g: Math.floor(lower.color.g * (1 - rangePct) + upper.color.g * rangePct),
            b: Math.floor(lower.color.b * (1 - rangePct) + upper.color.b * rangePct)
        };
    }
}
